package comp2526.lab1;

/**
 * Initialize 4 burger and adding dressing and ingredients into burger.
 * 
 * @author berong91
 * 
 */
public class BurgerShop {
    /**
     * So that nobody will create a reference to this class.
     */
    private BurgerShop() {

    }

    /**
     * Main method.
     * 
     * @param args
     *            main arguments
     */
    public static void main(String[] args) {
        // Initialize burger
        Burger burger1 = new ChickenBurger();
        Burger burger2 = new ChickenBurger();
        Burger burger3 = new VegieBurger();
        Burger burger4 = new VegieBurger();

        // Add dressing into burger
        burger1.addDressing(Burger.Dressing.Mio);
        burger1.addDressing(Burger.Dressing.Ranch);

        burger2.addDressing(Burger.Dressing.SweetOnion);

        burger3.addDressing(Burger.Dressing.None);
        burger3.addDressing(Burger.Dressing.Ranch);

        burger4.addDressing(Burger.Dressing.Ranch);
        burger4.addDressing(Burger.Dressing.Mio);
        burger4.addDressing(Burger.Dressing.SweetOnion);

        // Add ingredient into burger
        burger1.addIngredients(Burger.Ingredients.Lettice);
        burger1.addIngredients(Burger.Ingredients.Onion);

        burger2.addIngredients(Burger.Ingredients.Tomatoes);

        burger3.addIngredients(Burger.Ingredients.Lettice);
        burger3.addIngredients(Burger.Ingredients.Onion);
        burger3.addIngredients(Burger.Ingredients.Tomatoes);

        burger4.addIngredients(Burger.Ingredients.Onion);

        // Show packing results
        System.out.println("There are " + Burger.burgerCount
                + " burgers in our system.");
        System.out.println(burger1.pack());
        System.out.println(burger2.pack());
        System.out.println(burger3.pack());
        System.out.println(burger4.pack());
    }

}

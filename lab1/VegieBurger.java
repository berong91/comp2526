package comp2526.lab1;

/**
 * This class present a vegie burger.
 * 
 * @author berong91
 * 
 */
public class VegieBurger extends Burger {
    /**
     * Initialize a vegie burger.
     */
    public VegieBurger() {
        super();
    }

    @Override
    String pack() {
        String result = "Vegie Burger\nDressing\t";

        for (Dressing d : dressing) {
            result += d.getName() + "\t\t";
        }
        result += "\nIngredients\t";
        for (Ingredients i : ingredients) {
            result += i.getName() + "\t\t";
        }

        return result + "\n";
    }

}

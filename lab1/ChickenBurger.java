package comp2526.lab1;

/**
 * This class present a chicken burger.
 * 
 * @author berong91
 * 
 */
public class ChickenBurger extends Burger {
    /**
     * Initialize a chicken burger.
     */
    public ChickenBurger() {
        super();
    }

    @Override
    String pack() {
        String result = "Chicken Burger\nDressing\t";

        for (Dressing d : dressing) {
            result += d.getName() + "\t\t";
        }
        result += "\nIngredients\t";
        for (Ingredients i : ingredients) {
            result += i.getName() + "\t\t";
        }

        return result + "\n";
    }

}

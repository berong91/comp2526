package comp2526.lab1;

import java.util.HashSet;
import java.util.Set;

/**
 * Burger an abstract class.
 * 
 * @author berong91
 * 
 */
public abstract class Burger {
    /**
     * Dressing list.
     * 
     * @author berong91
     * 
     */
    public enum Dressing {
        /**
         * Dressing list.
         */
        Mio("Mio"), SweetOnion("Sweet Onion"), Ranch("Ranch"), None("None");

        /**
         * Name of this dressing.
         */
        private String name;

        /**
         * Constructor of this dressing.
         * 
         * @param name
         *            name of this burger
         */
        private Dressing(String name) {
            this.name = name;
        }

        /**
         * Return the name of this dressing.
         * 
         * @return name of this burger
         */
        String getName() {
            return name;
        }
    };

    /**
     * Ingredients list .
     * 
     * @author berong91
     * 
     */
    public enum Ingredients {
        /**
         * Ingredients list.
         */
        Lettice("Lettice"), Tomatoes("Tomatoes"), Onion("Onion");

        /**
         * Name of this ingredient.
         */
        private String name;

        /**
         * Constructor of this ingredient.
         * 
         * @param name
         *            name of this burger
         */
        private Ingredients(String name) {
            this.name = name;
        }

        /**
         * Return the name of this ingredient.
         * 
         * @return name of this burger
         */
        String getName() {
            return name;
        }
    };

    /**
     * Number of burger in total.
     */
    static int burgerCount;

    /**
     * Dressing list.
     */
    protected Set<Dressing> dressing = new HashSet<Dressing>();

    /**
     * Ingredient list.
     */
    protected Set<Ingredients> ingredients = new HashSet<Ingredients>();

    /**
     * Flag of dressing list.
     */
    private boolean dressingFlag = true;

    /**
     * Flag of ingredient list.
     */
    private boolean ingredientsFlag = true;

    /**
     * Initialize a burger.
     */
    public Burger() {
        burgerCount++;
    }

    /**
     * Packing this burger and show its' information.
     * 
     * @return info of burger
     */
    abstract String pack();

    /**
     * Add dressing into this burger.
     * 
     * @param d
     *            dressing
     */
    protected void addDressing(Dressing d) {
        if (dressingFlag) {
            dressing.add(d);
            dressingFlag = (d == Dressing.None || dressing.size() == 2) ? false
                    : true;
        }
    }

    /**
     * Add ingredient into this burger.
     * 
     * @param i
     *            ingredient
     */
    protected void addIngredients(Ingredients i) {
        if (ingredientsFlag) {
            ingredients.add(i);
            ingredientsFlag = (ingredients.size() == 2) ? false : true;
        }

    }

}
